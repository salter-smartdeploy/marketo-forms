# Marketo Forms

This is not a comprehensive list of all of SmartDeploy's Marketo forms. Just code from everything that's been customized.

## How to Add Custom Styles to a New Form

`custom.css` is applied to all forms, and the other css are each applied to a single form.

If you want to customize a new form:
1. Duplicate one of the existing html pages
2. Preview the form in Marketo, and copy/paste its `<form>` markup into the new duplicate html file
3. Replace the css link with your new css file. So you copied from contact-us.html, replace `<link href="./css/contact-us.css">` with `<link href="./css/new-form-name.css">`
4. View the html file in-browser and customize *only the CSS* as needed
4. Copy paste the styles from `custom.css` and your form-specific css into the Marketo form

## Don't Change Markup or Vendor Styles

This is just a test envorionment for building Marketo forms. The markup and the vendor styles are what Marketo generates and you won't be able to change them in the Marketo environment, so don't bother changing them here.